export PS1='\h:\w\$ '
export EDITOR=vi
alias ls='ls -FN'
alias vi='vim'
alias ygrep='C:\Program Files (x86)\Yokka\OuiEditor\Grep.exe'

alias ls='ls --color=auto --show-control-chars'
alias ll='ls --color=auto --show-control-chars -l'
