# 新カネハツProjectWEB
https://pjshr170.soln.jp/FJCLj7181GK/pjwebroot/index.jsp

ユーザ　　：inagaki.fumika
パスワード：inagaki.fumika@

## ■ＷＥＢサーバ（ＡＰ）
	URL: http://164.69.66.177/Base/common/login.aspx

## ログインID／PASS
1. KNH001 / KNH001	稲垣
2. KNH002 / KNH002 佐々木
3. KNH003 / KNH003 坂本
4. KNH004 / KNH004 廣川
5. KNH005 / KNH005 ＊＊＊＊

アプリケーションプール：FoodCORE_KNH
ソリューションID：FoodCOREKNH


## ■ＤＢサーバ（ＤＢ）
	サーバIP	：164.69.66.179
		APサーバより接続用IP：192.168.25.179

DB名:		FoodCORE_KNH
ログイン:	knh2
パスワード:	FoodCORE@01


## ■ソース（ＴＦＳ）
※ログインはVisual Studio 2010　のチームサーバから行ってください。

## ■ログインID／PASS
1. KNH001 / FoodCORE@01 稲垣
2. KNH002 / FoodCORE@02 佐々木
3. KNH003 / FoodCORE@03 坂本
4. KNH004 / FoodCORE@04 廣川
5. KNH005 / FoodCORE@05 ＊＊＊＊

## ■URL
http://164.69.69.7:8080/tfs

